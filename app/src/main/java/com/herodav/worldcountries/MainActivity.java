package com.herodav.worldcountries;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.herodav.worldcountries.model.Country;
import com.herodav.worldcountries.ui.ListCountryFragment;
import com.herodav.worldcountries.ui.ListCountryFragmentDirections;
import com.herodav.worldcountries.utils.ConnectivityUtils;
import com.herodav.worldcountries.utils.SnackBarUtils;

import static com.herodav.worldcountries.ui.CountryFragment.COUNTRY_NAME;

public class MainActivity extends AppCompatActivity implements ListCountryFragment.Callbacks {
    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.listCountryFragment
        ).build();

        NavController navController = Navigation.findNavController(this, R.id.fragment_container);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);

        //sets back the destination to listFragment on landscape mode
        if (navController.getCurrentDestination().getId() == R.id.countryFragment) {
            navController.navigate(R.id.listCountryFragment);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.fragment_container);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    @Override
    public void onCountrySelected(Country country) {
        if (ConnectivityUtils.isConnected(this)) {
            navigateToCountryDetail(country);
        } else {
            SnackBarUtils.showSnackbar(this, R.string.error_no_connection, R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dismiss
                }
            });
        }
    }

    private void navigateToCountryDetail(Country country) {
        Bundle bundle = new Bundle();
        bundle.putString(COUNTRY_NAME, country.getName());

        boolean isToPanes = getResources().getBoolean(R.bool.isTwoPane);

        if (isToPanes) {
            Navigation.findNavController(this, R.id.fragment_detail)
                    .navigate(R.id.countryFragment, bundle);
        } else {



            ListCountryFragmentDirections.ActionListCountryFragmentToCountryFragment action
                    = ListCountryFragmentDirections
                    .actionListCountryFragmentToCountryFragment(country.getName());

            Navigation.findNavController(this, R.id.fragment_container)
                    .navigate(action);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
