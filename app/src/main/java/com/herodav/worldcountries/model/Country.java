package com.herodav.worldcountries.model;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Country {

    @SerializedName("name")
    private String name;
    @SerializedName("callingCodes")
    private List<String> callingCodes;
    @SerializedName("capital")
    private String capital;
    @SerializedName("region")
    private String region;
    @SerializedName("subregion")
    private String subregion;
    @SerializedName("population")
    private long population;
    @SerializedName("area")
    private double area;
    @SerializedName("currencies")
    private List<Currency> currencies;
    @SerializedName("languages")
    private List<Language> languages;
    @SerializedName("flag")
    private String flagUrl;

    public Country(String name, List<String> callingCodes, String capital, String region,
                   String subregion, long population, double area, List<Currency> currencies,
                   List<Language> languages, String flagUrl) {
        this.name = name;
        this.callingCodes = callingCodes;
        this.capital = capital;
        this.region = region;
        this.subregion = subregion;
        this.population = population;
        this.area = area;
        this.currencies = currencies;
        this.languages = languages;
        this.flagUrl = flagUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCallingCodes() {
        return callingCodes;
    }

    public void setCallingCodes(List<String> callingCodes) {
        this.callingCodes = callingCodes;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSubregion() {
        return subregion;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public String getFlagUrl() {
        return flagUrl;
    }

    public void setFlagUrl(String flagUrl) {
        this.flagUrl = flagUrl;
    }

    public Uri getImageUri() {
        return Uri.parse(this.flagUrl);
    }

    public String getLanguagesAsString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < getLanguages().size(); i++) {
            builder.append(getLanguages().get(i).getName());
            if (i < getLanguages().size() - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public String getCurrenciesAsString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < getCurrencies().size(); i++) {
            builder.append(getCurrencies().get(i).getName())
                    .append(" (")
                    .append(getCurrencies().get(i).getSymbol())
                    .append(")");
            if (i < getCurrencies().size() - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();

    }

    @NonNull
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
