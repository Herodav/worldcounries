package com.herodav.worldcountries.ui;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.herodav.worldcountries.data.CountryRepository;
import com.herodav.worldcountries.model.Country;
import com.herodav.worldcountries.model.NetworkResource;

import java.util.List;

public class CountryViewModel extends AndroidViewModel {
    private CountryRepository mRepository;

    public CountryViewModel(Application application) {
        super(application);
        mRepository = new CountryRepository();
    }

    public LiveData<NetworkResource<List<Country>>> getAllCountries() {
        return mRepository.getAllCountries();
    }

    public MutableLiveData<NetworkResource<List<Country>>> getCountriesByName(String name) {
        return mRepository.getCountriesByName(name);
    }

    public MutableLiveData<NetworkResource<Country>> getSingleCountry(String name) {
        return mRepository.getSingleCountry(name);
    }
}
