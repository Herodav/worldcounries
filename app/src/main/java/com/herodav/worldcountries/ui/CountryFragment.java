package com.herodav.worldcountries.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;
import com.herodav.worldcountries.R;
import com.herodav.worldcountries.model.Country;
import com.herodav.worldcountries.model.NetworkResource;
import com.herodav.worldcountries.utils.ConnectivityUtils;
import com.herodav.worldcountries.utils.SnackBarUtils;

import static com.herodav.worldcountries.utils.Status.ERROR;
import static com.herodav.worldcountries.utils.Status.SUCCESS;

//todo: find out how to clear the stack before attaching this fragment
public class CountryFragment extends Fragment {
    private static final String TAG = CountryFragment.class.getSimpleName();

    public static final String COUNTRY_NAME = "country_name";
    private TextView tvName, tvCapital, tvRegion, tvSubregion, tvPopulation, tvArea, tvCurrencies,
            tvCode, tvLanguages;
    private ProgressBar mProgressBar;
    private ImageView imgFlag, imgPlaceHolder;
    private CountryViewModel mCountryViewModel;
    private Country mCountry;
    private String mCountryName;
    private ConstraintLayout mContainer;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_country, container, false);
        mProgressBar = (ProgressBar) v.findViewById(R.id.pb_detail);

        tvName = (TextView) v.findViewById(R.id.detail_tv_name);
        tvCapital = (TextView) v.findViewById(R.id.detail_tv_capital);
        tvRegion = (TextView) v.findViewById(R.id.detail_tv_region);
        tvSubregion = (TextView) v.findViewById(R.id.detail_tv_subregion);
        tvPopulation = (TextView) v.findViewById(R.id.detail_tv_population);
        tvArea = (TextView) v.findViewById(R.id.detail_tv_area);
        tvCurrencies = (TextView) v.findViewById(R.id.detail_tv_currency);
        tvCode = (TextView) v.findViewById(R.id.detail_tv_code);
        tvLanguages = (TextView) v.findViewById(R.id.detail_tv_languages);
        imgFlag = (ImageView) v.findViewById(R.id.detail_img_flag);
        imgPlaceHolder = (ImageView) v.findViewById(R.id.detail_img_place_holder);
        mContainer = (ConstraintLayout) v.findViewById(R.id.detail_container);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

        mCountryViewModel = new ViewModelProvider(requireActivity()).get(CountryViewModel.class);
        if (getArguments() != null) {
            mCountryName = CountryFragmentArgs.fromBundle(getArguments()).getCountryName();
        }
        if (mCountryName != null){
            getData();
        }
    }

    private void getData() {
        if (ConnectivityUtils.isConnected(getActivity())) {

            mCountryViewModel.getSingleCountry(mCountryName)
                    .observe(CountryFragment.this, new Observer<NetworkResource<Country>>() {
                        @Override
                        public void onChanged(NetworkResource<Country> resource) {
                            if (resource.status == SUCCESS && resource.data != null) {
                                mCountry = resource.data;

                            } else if (resource.status == ERROR) {
                                mCountry = null;
//                            Toast.makeText(getContext(), resource.message, Toast.LENGTH_LONG).show();
                            } else { //onFailure
                                mCountry = null;
                            }
                            updateUI();
                        }
                    });
        } else {
            SnackBarUtils.showSnackbar(getActivity(), R.string.error_no_connection, android.R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dismiss
                }
            });

        }
    }


    private void updateUI() {
        if (mCountry == null || mCountryName.isEmpty()) {
            imgPlaceHolder.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);

        } else {
            mProgressBar.setVisibility(View.GONE);

            mContainer.setVisibility(View.VISIBLE);
            imgPlaceHolder.setVisibility(View.GONE);

            GlideToVectorYou.justLoadImage(getActivity(), mCountry.getImageUri(), imgFlag);
            tvName.setText(mCountry.getName());
            tvCapital.setText(String.format(getString(R.string.detail_capital_city), mCountry.getCapital()));
            tvRegion.setText(String.format(getString(R.string.detail_region), mCountry.getRegion()));
            tvSubregion.setText(String.format(getString(R.string.detaill_subregion), mCountry.getSubregion()));
            tvPopulation.setText(String.format(getString(R.string.detail_population), mCountry.getPopulation()));
            tvArea.setText(String.format(getString(R.string.detail_area), (int) mCountry.getArea()));
            tvCurrencies.setText(String.format(getString(R.string.detail_currency), mCountry.getCurrenciesAsString()));
            tvCode.setText(String.format(getString(R.string.detail_code), mCountry.getCallingCodes()));
            tvLanguages.setText(String.format(getString(R.string.detail_languages), mCountry.getLanguagesAsString()));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCountryName = "yyy";
    }
}
