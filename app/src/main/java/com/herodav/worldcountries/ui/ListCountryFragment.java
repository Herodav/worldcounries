package com.herodav.worldcountries.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;
import com.herodav.worldcountries.R;
import com.herodav.worldcountries.model.Country;
import com.herodav.worldcountries.model.NetworkResource;
import com.herodav.worldcountries.utils.ConnectivityUtils;
import com.herodav.worldcountries.utils.SnackBarUtils;

import java.util.ArrayList;
import java.util.List;

import static com.herodav.worldcountries.utils.Status.ERROR;
import static com.herodav.worldcountries.utils.Status.SUCCESS;

public class ListCountryFragment extends Fragment {

    private CountryViewModel mCountryViewModel;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private CountryAdapter mAdapter;
    ImageView imgPlaceHolder;

    private List<Country> mCountryList;
    private Callbacks mCallbacks;

    /**
     * Required interface for hosting activities
     */
    public interface Callbacks {
        void onCountrySelected(Country country);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_country_fragment, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_countries);
        mProgressBar = (ProgressBar) v.findViewById(R.id.pb_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        imgPlaceHolder = (ImageView) v.findViewById(R.id.list_img_place_holder);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllData();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        mCountryViewModel = new ViewModelProvider(requireActivity()).get(CountryViewModel.class);
        getAllData();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getResources().getString(R.string.query_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String nextText) {
                mAdapter.getFilter().filter(nextText);
                return true;
            }
        });
    }

    private void getCountry(String query) {
        if (ConnectivityUtils.isConnected(getActivity())) {
            mCountryViewModel.getCountriesByName(query)
                    .observe(ListCountryFragment.this, new Observer<NetworkResource<List<Country>>>() {
                        @Override
                        public void onChanged(NetworkResource<List<Country>> resource) {
                            if (resource.status == SUCCESS && resource.data != null) {
                                mCountryList = resource.data;

                            } else if (resource.status == ERROR) {
                                mCountryList = null;
//                            Toast.makeText(getContext(), resource.message, Toast.LENGTH_LONG).show();
                            } else { //onFailure
                                mCountryList = null;
                            }
                            updateUI();
                        }
                    });
        } else {
            SnackBarUtils.showSnackbar(getActivity(), R.string.error_no_connection, android.R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dismiss
                }
            });

        }
    }

    private void getAllData() {
        if (ConnectivityUtils.isConnected(getActivity())) {
            mCountryViewModel.getAllCountries()
                    .observe(ListCountryFragment.this, new Observer<NetworkResource<List<Country>>>() {
                        @Override
                        public void onChanged(NetworkResource<List<Country>> resource) {
                            if (resource.status == SUCCESS && resource.data != null) {
                                mCountryList = resource.data;

                            } else if (resource.status == ERROR) {
                                mCountryList = null;

                            } else { //onFailure
                                mCountryList = null;
                            }
                            updateUI();
                        }
                    });
        } else {
            SnackBarUtils.showSnackbar(getActivity(), R.string.error_no_connection, android.R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dismiss
                }
            });

        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        getAllData();
    }

    public void updateUI() {

        if (mCountryList == null || mCountryList.size() == 0) {
            mProgressBar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            imgPlaceHolder.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.GONE);
            imgPlaceHolder.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            setUpRecyclerView();
        }
    }

    private void setUpRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new CountryAdapter(mCountryList);
        }
        mAdapter.setCountries(mCountryList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private class CountryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName;
        private TextView tvCapital;
        private TextView tvSubregion;
        private ImageView imgFlag;

        private Country mCountry;

        public CountryHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_country, parent, false));

            tvName = (TextView) itemView.findViewById(R.id.item_tv_name);
            tvCapital = (TextView) itemView.findViewById(R.id.item_tv_capital);
            tvSubregion = (TextView) itemView.findViewById(R.id.item_tv_subregion);
            imgFlag = (ImageView) itemView.findViewById(R.id.item_imv_flag);

            itemView.setOnClickListener(this);//itemView is the actual View
        }

        public void bind(Country country) {

            mCountry = country;
            tvName.setText(mCountry.getName());
            tvCapital.setText(mCountry.getCapital());
            tvSubregion.setText(mCountry.getSubregion());
            GlideToVectorYou.justLoadImage(getActivity(), mCountry.getImageUri(), imgFlag);
        }

        @Override
        public void onClick(View v) {
            mCallbacks.onCountrySelected(mCountry);
        }
    }

    private class CountryAdapter extends RecyclerView.Adapter<CountryHolder> implements Filterable {
        private List<Country> mCountries = new ArrayList<>();;
        private List<Country> mFilteredCountries =  new ArrayList<>();

        public CountryAdapter(List<Country> countries) {
            mCountries = countries;
            mFilteredCountries = countries;
        }

        @NonNull
        @Override
        public CountryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            return new CountryHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull CountryHolder holder, int position) {
            Country country = mFilteredCountries.get(position);
            holder.bind(country);
        }

        private void setCountries(List<Country> countries) {
            mFilteredCountries = countries;
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return mFilteredCountries.size();
        }


        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    String charString = constraint.toString();
                    if (charString.isEmpty()) {
                        mFilteredCountries = mCountries;
                    } else{
                        ArrayList<Country> filteredList = new ArrayList<>();
                        for (Country country : mCountries) {
                            if (country.getName().toLowerCase().contains(charString.toLowerCase())){
                                filteredList.add(country);
                            }
                        }
                        mFilteredCountries = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mFilteredCountries;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mFilteredCountries = (ArrayList<Country>) results.values;
                    notifyDataSetChanged(); //the list is refreshed here
                }
            };
        }
    }

}
