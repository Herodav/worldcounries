package com.herodav.worldcountries.data;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.herodav.worldcountries.data.network.CountryService;
import com.herodav.worldcountries.data.network.ServiceGenerator;
import com.herodav.worldcountries.model.Country;
import com.herodav.worldcountries.model.NetworkResource;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.herodav.worldcountries.utils.Constants.EMPTY_RESPONSE;
import static com.herodav.worldcountries.utils.Constants.NETWORK_ERROR;

public class CountryRepository {
    private static final String TAG = CountryRepository.class.getSimpleName();

    private MutableLiveData<NetworkResource<List<Country>>> mCountries;
    private MutableLiveData<NetworkResource<Country>> mCountry;
    private CountryService mCountryService;


    public CountryRepository() {
        mCountries = new MutableLiveData<>();
        mCountry = new MutableLiveData<>();
        mCountryService = ServiceGenerator.createService(CountryService.class);
    }

    public MutableLiveData<NetworkResource<List<Country>>> getAllCountries() {

        mCountryService.getAllCountries().enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        mCountries.setValue(NetworkResource.success(response.body()));
                    } else {
                        mCountries.setValue(NetworkResource.error(EMPTY_RESPONSE, response.body()));
                    }
                } else {
                    mCountries.setValue(NetworkResource.error(NETWORK_ERROR, response.body()));
                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);

//                mCountries.setValue(NetworkResource.error(CONNECCTION_FAILURE, null));
            }
        });

        return mCountries;
    }


    public MutableLiveData<NetworkResource<List<Country>>> getCountriesByName(String name) {

        mCountryService.getCountriesByName(name).enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        //get first index since the API returns an array
                        mCountries.setValue(NetworkResource.success(response.body()));
                    } else {
                        mCountries.setValue(NetworkResource.error(EMPTY_RESPONSE,
                                response.body()));
                    }
                } else {
                    mCountries.setValue(NetworkResource.error(EMPTY_RESPONSE,
                            response.body()));
                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });

        return mCountries;
    }

    public MutableLiveData<NetworkResource<Country>> getSingleCountry(String name) {

        mCountryService.getSingleCountry(name).enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        //get first index since the API returns an array
                        mCountry.setValue(NetworkResource.success(response.body().get(0)));
                    } else {
                        mCountry.setValue(NetworkResource.error(EMPTY_RESPONSE,
                                response.body() != null ? response.body().get(0) : null));
                    }
                } else {
                    mCountry.setValue(NetworkResource.error(EMPTY_RESPONSE,
                            response.body() != null ? response.body().get(0) : null));
                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });

        return mCountry;
    }
}
