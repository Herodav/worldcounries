package com.herodav.worldcountries.data.network;

import com.herodav.worldcountries.model.Country;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static com.herodav.worldcountries.utils.Endpoints.ALL;
import static com.herodav.worldcountries.utils.Endpoints.BY_NAME_MULTIPLE;
import static com.herodav.worldcountries.utils.Endpoints.BY_NAME_SINGLE;

public interface CountryService {

    @GET(ALL)
    Call<List<Country>> getAllCountries();

    @GET(BY_NAME_SINGLE)
    Call<List<Country>> getSingleCountry(@Path("name") String name);

    @GET(BY_NAME_MULTIPLE)
    Call<List<Country>> getCountriesByName(@Path("name") String name);


}
