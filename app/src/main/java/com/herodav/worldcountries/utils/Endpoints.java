package com.herodav.worldcountries.utils;

public class Endpoints {

    public static final String BASE_URL = "https://restcountries.eu/rest/v2/";


    public static final String ALL = "all?fields=name;flag;capital;subregion";
    public static final String BY_NAME_SINGLE = "name/{name}?fullText=tru";
    public static final String BY_NAME_MULTIPLE = "name/{name}";
}
