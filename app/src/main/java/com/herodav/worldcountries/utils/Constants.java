package com.herodav.worldcountries.utils;

public class Constants {

    public static final String NETWORK_ERROR = "Network error";
    public static final String EMPTY_RESPONSE = "Empty response";

}
